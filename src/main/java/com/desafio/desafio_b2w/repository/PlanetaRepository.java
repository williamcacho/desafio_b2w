/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.desafio.desafio_b2w.repository;


import com.desafio.desafio_b2w.model.Planeta;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *
 * @author william.cacho
 */
public interface PlanetaRepository extends MongoRepository<Planeta, String> {
  Planeta findBy_id(ObjectId _id);
}
