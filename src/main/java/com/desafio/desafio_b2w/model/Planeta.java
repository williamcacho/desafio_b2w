/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.desafio.desafio_b2w.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

/**
 *
 * @author william.cacho
 */
public class Planeta {
    
    @Id
    private ObjectId _id;
    private String nome;
    private String clima;
    private String terreno;

    public Planeta(ObjectId _id, String nome, String clima, String terreno) {
        this._id = _id;
        this.nome = nome;
        this.clima = clima;
        this.terreno = terreno;
    }


    public ObjectId getId() {
        return _id;
    }

    public void setId(ObjectId _id) {
        this._id = _id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getClima() {
        return clima;
    }

    public void setClima(String clima) {
        this.clima = clima;
    }

    public String getTerreno() {
        return terreno;
    }

    public void setTerreno(String terreno) {
        this.terreno = terreno;
    }

    
 
    
}
