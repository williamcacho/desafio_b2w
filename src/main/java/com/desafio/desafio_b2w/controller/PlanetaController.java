/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.desafio.desafio_b2w.controller;


import com.desafio.desafio_b2w.model.Planeta;
import com.desafio.desafio_b2w.repository.PlanetaRepository;

import java.util.List;
import javax.validation.Valid;
import org.bson.types.ObjectId;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author william.cacho
 */
@RestController
@RequestMapping("/Planeta")
public class PlanetaController {
  @Autowired
  private PlanetaRepository repository;
  
  @RequestMapping(value = "/", method = RequestMethod.GET)
public List<Planeta> getAllPlaneta() {
  return repository.findAll();
}
@RequestMapping(value = "/{id}", method = RequestMethod.GET)

public Planeta getPlanetaById(@PathVariable("id") ObjectId id) {
  return repository.findBy_id(id);
}

@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
public void modifyPlanetaById(@PathVariable("id") ObjectId id, @Valid 
@RequestBody Planeta planeta) {
  planeta.setId(id);
  repository.save(planeta);
}

@RequestMapping(value = "/", method = RequestMethod.POST)
public Planeta createPlaneta(@Valid @RequestBody Planeta planeta) {
  planeta.setId(ObjectId.get());
  repository.save(planeta);
  return planeta;
}
@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
public void deletePlaneta(@PathVariable ObjectId id) {
  repository.delete(repository.findBy_id(id));
}

}